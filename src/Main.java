import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {
    static final String ENTER_NAME = "Enter your name:";
    static final String ERROR_NAME = "Incorrect name!";
    static final String ENTER_NUMBER = "Enter number series:";
    static final String ERROR_NUMBER = "Incorrect number series!";
    static final String OUTPUT_MSG = "Sum : %d, Product: %d";
    static final String HELLO_MSG = "Hello, %s!";

    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String name = ValidInput.getString(bufferedReader, ENTER_NAME, ERROR_NAME);

        System.out.println(String.format(HELLO_MSG, name));

        int[] numbers = ValidInput.getIntegerArray(bufferedReader, ENTER_NUMBER, ERROR_NUMBER);

        System.out.println(String.format(
                OUTPUT_MSG,
                Arrays.stream(numbers).sum(),
                Arrays.stream(numbers).reduce(1, (a, b) -> a * b)));
    }
}
