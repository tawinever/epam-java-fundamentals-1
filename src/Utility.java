import java.util.Arrays;

public class Utility {
    public static int[] parseNumbers(String rawNumbers) {
        String[] nums = rawNumbers.trim().split("\\s+");
        return Arrays.stream(nums).mapToInt(Integer::parseInt).toArray();
    }
}
