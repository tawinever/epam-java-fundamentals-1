import java.io.BufferedReader;
import java.io.IOException;

public class ValidInput {
    public static String getString(BufferedReader bufferedReader, String message, String error) {
        String input = null;
        while (input == null || input.trim().isEmpty()) {
            System.out.println(message);
            try {
                input = bufferedReader.readLine();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                continue;
            }
            if(input != null && input.isEmpty()) {
                System.out.println(error);
            }
        }
        return input;
    }

    public static int[] getIntegerArray(BufferedReader bufferedReader, String message, String error) {
        String rawNumbers = null;
        int[] integerArray = null;

        while (rawNumbers == null) {
            try {
                System.out.println(message);
                rawNumbers = bufferedReader.readLine();
                integerArray = Utility.parseNumbers(rawNumbers);
                if (integerArray.length > 0) {
                    break;
                }
            } catch (IOException|NumberFormatException e) {
                System.out.println(error);
                rawNumbers = null;
            }
        }
        return integerArray;
    }
}
